#!/bin/sh
STAGE=${1:-"STAGING"}

echo $0
bash --version
sh --version

rm dist/*.*
printf "$(cat version.txt).$BITBUCKET_BUILD_NUMBER" > version.txt

pip install 'setuptools>=41.0.1'
pip install 'wheel>=0.33.4'
pip install 'JSON-log-formatter>=0.5.1'

python setup.py sdist

pip install 'twine>=1.11.0'

if [ $STAGE = "PRODUCTION" ]; then
    twine upload --skip-existing dist/*
else
    twine upload --skip-existing --repository-url https://test.pypi.org/legacy/ dist/* --verbose
fi
