.. toctree::
   :maxdepth: 2
   :caption: Contents:

This code is licensed under MIT license. See license.txt_ for details.

.. _license.txt: https://bitbucket.org/deductive/newtools/src/master/licence.txt

Provides useful libraries for processing large data sets.
Developed by the team at deductive.com_ as we find them useful in our projects.

.. _deductive.com: https://deductive.com

You can view the source code here_

.. _here: https://bitbucket.org/deductive/newtools/

Installation
------------

Install using PIP by using

.. code-block:: bash

    pip install newtools

The default install does not include any of the dependent libraries and runtime errors will be raised if the required libraries are not included.

You can install all dependencies as follows:

.. code-block:: bash

    pip install newtools[full]



S3Location class
----------------

.. autoclass:: newtools.S3Location
   :members:
   :undoc-members:
   :show-inheritance:


AthenaClient class
------------------

.. autoclass:: newtools.db.athena.AthenaClient
   :members:
   :undoc-members:
   :show-inheritance:

Cached Queries
--------------

.. automodule:: newtools.db.cached_query
   :members:
   :undoc-members:
   :show-inheritance:

SQLClient class
---------------

.. autoclass:: newtools.db.sql_client.SqlClient
   :members:
   :undoc-members:
   :show-inheritance:

LoadPartitions class
---------------------

.. autoclass:: newtools.aws.load_partitions.LoadPartitions
   :members:
   :undoc-members:
   :show-inheritance:


CSV Doggo
---------
.. automodule:: newtools.doggo.csv
   :members:
   :undoc-members:
   :show-inheritance:

File Doggo
----------
.. autoclass:: newtools.doggo.FileDoggo
   :members:
   :undoc-members:
   :show-inheritance:

Pandas Doggo
------------

.. autoclass:: newtools.doggo.doggo.PandasDoggo
   :members:
   :undoc-members:
   :show-inheritance:

Doggo File System
-----------------

.. autoclass:: newtools.doggo.fs.DoggoFileSystem
   :members:
   :undoc-members:
   :show-inheritance:

Doggo Locks
-----------

.. autoclass:: newtools.doggo.lock.DoggoLock
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: newtools.doggo.lock.DynamoDogLock
   :members:
   :undoc-members:
   :show-inheritance:

Doggo Wait
----------

.. autoclass:: newtools.doggo.lock.DoggoWait
   :members:
   :undoc-members:
   :show-inheritance:

Logging
-------

.. autoclass:: newtools.log.log.log_to_stdout
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: newtools.log.persistent_field_logger
   :members:
   :undoc-members:
   :show-inheritance:

Task Queue
----------

.. autoclass:: newtools.queue.task_queue.TaskQueue

Search
======

* :ref:`search`
