import unittest
from os import path

from s3fs import S3FileSystem

from newtools import AthenaClient, log_to_stdout, S3Location
from newtools.db.data_types import *

logger = log_to_stdout("newtools")


# noinspection SqlNoDataSourceInspection
class AthenaTest(unittest.TestCase):
    source_file = "s3://newtools-tests/s3_upload_athena/demographic/"
    tablename = "demographic"
    region = "us-west-2"
    database = "test_db"
    bucket = "newtools-tests"
    output_location = "s3://newtools-tests/query_results/"
    invalid_file_location = "/this/location/does/not/exist.csv"
    athena_client = None

    @classmethod
    def setUpClass(cls):
        logger.info("Running setupclass for Athena test")
        cls.base_path = "{0}/test_data/".format(path.dirname(path.abspath(__file__)))
        cls.test_data_dir = path.join(cls.base_path, 'create_tables_athena')

        logger.info("Uploading test data to s3")
        cls.s3 = S3FileSystem()
        for file in cls.s3.glob(f"s3://{cls.bucket}/s3_upload_athena/*"):
            cls.s3.rm(file)

        cls.s3.put(lpath=path.join(cls.base_path, "s3_upload_athena"),
                   rpath=f"s3://{cls.bucket}/s3_upload_athena",
                   recursive=True)

        cls.athena_client = AthenaClient(cls.region, cls.database, max_retries=2)

        logger.info("Creating Table: {}".format(cls.tablename))
        cls.athena_client.add_query(
            f"""
            CREATE EXTERNAL TABLE IF NOT EXISTS {cls.tablename}(
              jurisdiction_name integer,
              count_participants integer,
              count_female integer,
              percent_female float,
              count_male integer,
              percent_male float)
            PARTITIONED BY (`part` string)
            ROW FORMAT SERDE
              'org.apache.hadoop.hive.serde2.OpenCSVSerde'
            STORED AS INPUTFORMAT
              'org.apache.hadoop.mapred.TextInputFormat'
            OUTPUTFORMAT
              'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
            LOCATION
              '{cls.source_file}'
            TBLPROPERTIES ('skip.header.line.count'='1')
            ;""", output_location=cls.output_location)

        cls.athena_client.add_query(
            f"""
            ALTER TABLE {cls.tablename} ADD IF NOT EXISTS PARTITION (part="1")
            LOCATION '{cls.source_file}part=1/'
            ;""", output_location=cls.output_location)

        cls.athena_client.add_query(
            f"""
            ALTER TABLE {cls.tablename} ADD IF NOT EXISTS PARTITION (part="2")
            LOCATION '{cls.source_file}part=2/'
            """, output_location=cls.output_location)

        cls.athena_client.wait_for_completion()

    @classmethod
    def tearDownClass(cls):
        cls.athena_client.add_query(f"DROP TABLE {cls.tablename}", output_location=cls.output_location)
        cls.athena_client.wait_for_completion()

    def _load_sql(self, sql_name: str) -> str:
        with open(path.join(self.test_data_dir, f'{sql_name}.sql'), 'r') as f:
            return f.read()

    def test_queries(self):
        logger.info("Querying athena")
        for file in self.s3.glob(f"s3://{self.bucket}/pq/*"):
            self.s3.rm(file)

        self.athena_client.add_query("select * from {}".format(self.tablename),
                                     "athena_test",
                                     "{0}pq/".format(self.output_location))
        count = self.athena_client.add_query("select count(*) from {}".format(self.tablename),
                                             "athena_test",
                                             self.output_location)
        self.athena_client.wait_for_completion()

        # self.assertEqual(0, self.scp.number_active)
        self.assertEqual(0, self.athena_client.number_active)

        df_count = self.athena_client.get_query_result(count)
        self.assertEqual(df_count.iloc[0, 0], 42)

    def test_stopping_queries(self):
        for file in self.s3.glob(f"s3://{self.bucket}/pq/*"):
            self.s3.rm(file)

        ac = AthenaClient(self.region, self.database, max_retries=2, max_queries=1)
        ac.add_query("select * from {}".format(self.tablename),
                     "athena_test",
                     "{0}pq/".format(self.output_location))
        ac.add_query("select count(*) from {}".format(self.tablename),
                     "athena_test",
                     self.output_location)
        self.assertEqual(1, ac.number_active)
        self.assertEqual(1, ac.number_pending)
        ac.stop_and_delete_all_tasks()
        self.assertEqual(0, ac.number_active)

    def test_raise_athena_exception(self):
        logger.info("Checking if AthenaClient throws exception when a request\
                     for results of an incomplete query is made")

        query = self.athena_client.add_query("select * from {}".format(self.tablename),
                                             "athena_test",
                                             self.output_location)
        with self.assertRaises(ValueError):
            self.athena_client.get_query_result(query)

    def test_reset_query(self):
        """Test to check if is_complete  attribute for a failed query is
        set after checking query status"""
        with self.assertRaises(TimeoutError):
            self.athena_client.add_query("select {0} from {1}".format("table_doesnt_exist", self.tablename),
                                         "athena_test",
                                         self.output_location)
            self.athena_client.wait_for_completion()

    def test_maximum_retries_imposed(self):
        """Test to check query doesn't exceed maximum retries"""
        with self.assertRaises(TimeoutError):
            query = self.athena_client.add_query("select {0} from {1}".format("table_doesnt_exist", self.tablename),
                                                 "athena_test",
                                                 self.output_location)
            self.athena_client.wait_for_completion()

        self.athena_client._update_task_status(query)
        self.assertEqual(query.retries, 2)
        self.assertTrue(query.is_complete)
        self.assertEqual(
            query.error,
            "COLUMN_NOT_FOUND: line 1:8: Column 'table_doesnt_exist' cannot be resolved or requester is not authorized "
            "to access requested resources"
        )

    def test_run_query_after_failure(self):
        """Test exception"""

        try:
            self.athena_client.add_query("not sql 23r739",
                                         "athena_test",
                                         self.output_location)
            self.athena_client.wait_for_completion()
        except Exception:
            pass
        finally:
            query = self.athena_client.add_query("select 0",
                                                 "athena_test",
                                                 self.output_location)
            self.athena_client.wait_for_completion()
            self.athena_client.get_query_result(query)

    def test_get_partition_projection_props_date_default(self) -> None:
        output = self.athena_client.get_partition_projection_props(
            column='COLUMN',
            metadata={
                'type': 'date'
            }
        )

        self.assertEqual(output, {
            'projection.COLUMN.format': 'yyyy-MM-dd',
            'projection.COLUMN.interval': '1',
            'projection.COLUMN.interval.unit': 'DAYS',
            'projection.COLUMN.range': 'NOW-2YEARS,NOW',
            'projection.COLUMN.type': 'date'
        })

    def test_get_partition_projection_props_date_lookback(self) -> None:
        output = self.athena_client.get_partition_projection_props(
            column='COLUMN',
            metadata={
                'type': 'date',
                'lookback': '1MONTH'
            }
        )

        self.assertEqual(output, {
            'projection.COLUMN.format': 'yyyy-MM-dd',
            'projection.COLUMN.interval': '1',
            'projection.COLUMN.interval.unit': 'DAYS',
            'projection.COLUMN.range': 'NOW-1MONTH,NOW',
            'projection.COLUMN.type': 'date'
        })

    def test_get_partition_projection_props_enum_empty(self) -> None:
        with self.assertRaisesRegex(
            AssertionError,
            "No enum values have been provided for partition projection on column: COLUMN"
        ):
            self.athena_client.get_partition_projection_props(
                column='COLUMN',
                metadata={
                    'type': 'enum'
                }
            )

    def test_get_partition_projection_props_enum_not_list(self) -> None:
        with self.assertRaisesRegex(
            AssertionError,
            "The values for an enum partition projection must be a LIST"
        ):
            self.athena_client.get_partition_projection_props(
                column='COLUMN',
                metadata={
                    'type': 'enum',
                    'values': 'val1'
                }
            )

    def test_get_partition_projection_props_enum(self) -> None:
        output = self.athena_client.get_partition_projection_props(
            column='COLUMN',
            metadata={
                'type': 'enum',
                'values': ['val1', 'val2']
            }
        )

        self.assertEqual(output, {
            'projection.COLUMN.values': 'val1,val2',
            'projection.COLUMN.type': 'enum'
        })

    def test_get_partition_projection_props_not_supported(self) -> None:
        with self.assertRaisesRegex(
            ValueError,
            "Partition projection type UNSUPPORTED is not supported"
        ):
            self.athena_client.get_partition_projection_props(
                column='COLUMN',
                metadata={
                    'type': 'UNSUPPORTED'
                }
            )

    def test_get_tbl_props_parquet(self) -> None:
        output = self.athena_client.get_tbl_props(file_format='PARQUET')
        self.assertEqual(
            output,
            "'classification' = 'parquet',\n  "
            "'compressionType' = 'snappy'"
        )

    def test_get_tbl_props_csv(self) -> None:
        output = self.athena_client.get_tbl_props(file_format='CSV')
        self.assertEqual(
            output,
            "'skip.header.line.count' = '1'"
        )

    def test_get_tbl_props_parquet_part_proj(self) -> None:
        output = self.athena_client.get_tbl_props(
            file_format='PARQUET',
            partition_proj={
                'COL1': {
                    'type': 'enum',
                    'values': ['val1', 'val2']
                },
                'COL2': {
                    'type': 'date'
                }
            }
        )
        self.assertEqual(
            output,
            "'classification' = 'parquet',\n  "
            "'compressionType' = 'snappy',\n  "
            "'projection.enabled' = 'TRUE',\n  "
            "'projection.COL1.values' = 'val1,val2',\n  "
            "'projection.COL1.type' = 'enum',\n  "
            "'projection.COL2.format' = 'yyyy-MM-dd',\n  "
            "'projection.COL2.interval' = '1',\n  "
            "'projection.COL2.interval.unit' = 'DAYS',\n  "
            "'projection.COL2.range' = 'NOW-2YEARS,NOW',\n  "
            "'projection.COL2.type' = 'date'"
        )

    def test_get_rfs_parquet(self) -> None:
        output = self.athena_client.get_rfs('PARQUET')
        self.assertEqual(output, 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe')

    def test_get_rfs_csv(self) -> None:
        output = self.athena_client.get_rfs('CSV')
        self.assertEqual(output, 'org.apache.hadoop.hive.serde2.OpenCSVSerde')

    def test_get_column_schema_invalid_type(self) -> None:
        with self.assertRaisesRegex(
            AssertionError,
            "Column types .'string'. must be one of the supported types: "
            ".'BooleanType', 'IntegerType', 'BigIntType', 'DoubleType', 'FloatType', "
            "'DecimalType', 'StringType', 'VarCharType', 'TimestampType', 'DateType'."
        ):
            self.athena_client.get_column_schema(
                column_schema={
                    'COL1': StringType(),
                    'COL2': 'string'
                }
            )

    def test_get_column_schema(self) -> None:
        output = self.athena_client.get_column_schema(
            column_schema={
                'COL1': BooleanType(),
                'COL2': IntegerType(),
                'COL3': BigIntType(),
                'COL4': DoubleType(),
                'COL5': FloatType(),
                'COL6': DecimalType(10, 5),
                'COL7': StringType(),
                'COL8': VarCharType(200),
                'COL9': TimestampType(),
                'COL10': DateType()
            }
        )

        self.assertEqual(
            output,
            'COL1 boolean,\n  '
            'COL2 integer,\n  '
            'COL3 bigint,\n  '
            'COL4 double,\n  '
            'COL5 float,\n  '
            'COL6 decimal(10, 5),\n  '
            'COL7 string,\n  '
            'COL8 varchar(200),\n  '
            'COL9 timestamp,\n  '
            'COL10 date'
        )

    def test_get_ct_query_wrong_format(self) -> None:
        with self.assertRaisesRegex(AssertionError, "The file format must be one of CSV or PARQUET, not UNKNOWN"):
            self.athena_client.get_ct_query(
                table_name='test_table',
                s3_location=S3Location('test-bucket'),
                column_schema={},
                file_format='UNKNOWN'
            )

    def test_get_ct_query_no_partitions(self) -> None:
        output = self.athena_client.get_ct_query(
            table_name='test_table',
            s3_location=S3Location('test-bucket').join('test_prefix'),
            column_schema={
                'COL1': StringType(),
                'COL2': TimestampType(),
                'COL3': BooleanType()
            },
            file_format='CSV'
        )
        exp_result = self._load_sql('no_partitions')
        self.assertEqual(output, exp_result)

    def test_get_ct_query_partitions_no_projection(self) -> None:
        output = self.athena_client.get_ct_query(
            table_name='test_table',
            s3_location=S3Location('test-bucket').join('test_prefix'),
            db_name='test_input_database',
            column_schema={
                'COL1': IntegerType(),
                'COL2': TimestampType()
            },
            partition_columns={
                'COL3': StringType()
            }
        )
        exp_result = self._load_sql('partitions_no_projection')
        self.assertEqual(output, exp_result)

    def test_get_ct_query_partitions_with_projection(self) -> None:
        output = self.athena_client.get_ct_query(
            table_name='test_table',
            s3_location=S3Location('test-bucket').join('test_prefix'),
            column_schema={
                'COL1': IntegerType(),
                'COL2': TimestampType()
            },
            partition_columns={
                'COL3': StringType(),
                'COL4': StringType()
            },
            partition_projection={
                'COL3': {
                    'type': 'date'
                },
                'COL4': {
                    'type': 'enum',
                    'values': [
                        'VAL1',
                        'VAL2'
                    ]
                }
            }
        )
        exp_result = self._load_sql('partitions_with_projection')
        self.assertEqual(output, exp_result)

    def test_get_ct_query_missing_part_proj_info(self) -> None:
        with self.assertRaisesRegex(AssertionError, "Columns {'COL2'} do not projection information"):
            self.athena_client.get_ct_query(
                table_name='test_table',
                s3_location=S3Location('test-bucket'),
                column_schema={},
                partition_columns={
                    'COL1': StringType(),
                    'COL2': StringType()
                },
                partition_projection={
                    'COL1': {}
                }
            )

    def test_string_repr(self) -> None:
        self.assertEqual(
            'String',
            StringType().__repr__()
        )

    def test_decimal_repr(self) -> None:
        self.assertEqual(
            'Decimal(10, 5)',
            DecimalType(10, 5).__repr__()
        )

    def test_varchar_repr(self) -> None:
        self.assertEqual(
            'VarChar(10)',
            VarCharType(10).__repr__()
        )
