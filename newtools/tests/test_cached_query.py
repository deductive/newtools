# (c) Deductive 2012-2020, all rights reserved
# This code is licensed under MIT license (see license.txt for details)

import os
import re
import sqlite3
from datetime import datetime, timedelta
from time import time, sleep
from typing import List
from unittest import mock

import pandas as pd

from newtools import (
    CachedPep249Query,
    CachedAthenaQuery,
    BaseCachedQuery,
    PandasDoggo,
    DoggoFileSystem,
    CachedCTASQuery,
    S3Location
)
from newtools.db.cached_query import UselessLock
from newtools.doggo.lock import DynamoDogLock, DoggoLock
from newtools.tests.base_test import BaseTest


class TestCachedQuery(BaseTest):
    region = "us-west-2"
    athena_db_name = "sampledb"

    @classmethod
    def setUpClass(cls):
        cls.s3_bucket = "s3://newtools-tests/query-results/{0}/".format(time())
        super(TestCachedQuery, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        dfs = DoggoFileSystem()
        for file in dfs.glob(cls.s3_bucket + "**"):
            dfs.rm(file)
        super(TestCachedQuery, cls).tearDownClass()

    def setUp(self):
        self.sqlite_db = sqlite3.connect(os.path.join(self.test_dir, "query_tests", "test.sqlite"))
        self.sql_path = os.path.join(self.test_dir, "query_tests")
        super(TestCachedQuery, self).setUp()

    @staticmethod
    def set_timeouts(q):
        q.wait_period = 0
        q.time_out_seconds = 1
        q.maximum_age = 120

    def test_useless_mock(self) -> None:
        with self.assertLogs('newtools.cached_query.UselessLock') as logs:
            with UselessLock(1, b=2):
                self.assertIn('INFO:newtools.cached_query.UselessLock:Acquiring useless lock', logs.output)
            self.assertIn('INFO:newtools.cached_query.UselessLock:Releasing useless lock', logs.output)

    def test_correct_mock(self) -> None:
        self.assertEqual(BaseCachedQuery(use_lock=False)._aws_lock, UselessLock)
        self.assertEqual(BaseCachedQuery(dynamodb_lock=True)._aws_lock, DynamoDogLock)
        self.assertEqual(BaseCachedQuery(dynamodb_lock=False)._aws_lock, DoggoLock)

    def test_bad_path_pep249(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              gzip=False)
        self.set_timeouts(q)

        with self.assertRaises(ValueError):
            q.get_results(sql_file="bad_query.sql",
                          output_prefix="test_query",
                          )

    def test_log_pep249(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              gzip=False)
        self.set_timeouts(q)

        with self.assertLogs("newtools.cached_query", level="INFO") as cm:
            q.get_results(sql_file="test_select.sql",
                          output_prefix="test_query_1_row",
                          params={
                              "secret": "my secret"
                          }
                          )

        self.assertIn('INFO:newtools.cached_query:secret = *********', cm.output)

    def test_log_pep249_string_path(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=self.sql_path,
                              gzip=False)
        self.set_timeouts(q)

        with self.assertLogs("newtools.cached_query", level="INFO") as cm:
            q.get_results(sql_file="test_select.sql",
                          output_prefix="test_query_1_row",
                          params={
                              "secret": "my secret"
                          }
                          )

        self.assertIn('INFO:newtools.cached_query:secret = *********', cm.output)

    def test_cache_pep249(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              gzip=False)
        self.set_timeouts(q)

        a = q.get_results(sql_file="test_select.sql",
                          output_prefix="test_query",
                          )

        # rerun the get_results to get full coverage
        a1 = q.get_results(sql_file="test_select.sql",
                           output_prefix="test_query",
                          )
        self.assertEqual(a, a1)

        # Validate the results still work without a database connection
        q2 = BaseCachedQuery(cache_path=self.tempdir,
                             sql_paths=[self.sql_path],
                             gzip=False)
        self.set_timeouts(q2)

        b = q2.get_results(sql_file="test_select.sql",
                           output_prefix="test_query",
                           )

        # Now clear the cache and check it doesn't work
        q.clear_cache(sql_file="test_select.sql",
                      output_prefix="test_query")

        with self.assertRaisesRegex(NotImplementedError, "BaseCachedClass cannot execute queries"):
            q2.get_results(sql_file="test_select.sql",
                           output_prefix="test_query",
                           )

        self.assertEqual(a, b)

    def test_query_expiring_cache(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              expiry_seconds=3,
                              gzip=False)
        self.set_timeouts(q)

        # get to the point where a new three-second period has started...
        if int(int(time())) % 3 == 1:
            sleep(1)

        path1 = q.get_results(sql_file="test_select.sql",
                              output_prefix="test_query",
                              )

        path2 = q.get_results(sql_file="test_select.sql",
                              output_prefix="test_query",
                              )
        sleep(3)

        path3 = q.get_results(sql_file="test_select.sql",
                              output_prefix="test_query",
                              )

        # Check the two queries run in the same period use the same path
        self.assertEqual(path1, path2)

        # Check the second query returns a different path
        self.assertNotEqual(path1, path3)

    def test_query_pep249(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              gzip=False)
        self.set_timeouts(q)

        q.get_results(sql_file="test_select.sql",
                      output_prefix="test_query",
                      )

        q.get_results(sql_file="test_select_in.sql",
                      output_prefix="test_query_1_row",
                      params={
                          "test_value": 2
                      }
                      )

        q.get_results(sql_file="test_select_in.sql",
                      output_prefix="test_query_0_rows",
                      params={
                          "test_value": 1
                      }
                      )

        self.compare_files(os.path.join(self.test_dir, "query_tests", "output"))

    def test_query__new_archive_path_pep249(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              params={"sql_archive_path": os.path.join(self.tempdir, "new")},
                              gzip=False)
        self.set_timeouts(q)

        q.get_results(sql_file="test_select.sql",
                      output_prefix="test_query"
                      )

    def test_1_row_as_param_to_obj_pep249(self):
        q = CachedPep249Query(self.sqlite_db,
                              params={
                                  "test_value": 2
                              },
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              gzip=False)
        self.set_timeouts(q)

        q.get_results(sql_file="test_select_in.sql",
                      output_prefix="test_query_1_row",
                      )

        self.compare_files(os.path.join(self.test_dir, "query_tests", "output"))

    def test_with_s3_path_pep249(self):
        # note we don't explicitly test using Redshift as we don't have one available for testing
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              gzip=False)
        self.set_timeouts(q)

        q.get_results(sql_file="test_select_s3_path.sql",
                      output_prefix="test_query_0_rows",
                      params={
                          "s3_path": 1
                      }
                      )

        self.compare_files(os.path.join(self.test_dir, "query_tests", "output"))

    def test_archive_queries_pep249(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              sql_archive_path=self.tempdir,
                              gzip=False)
        self.set_timeouts(q)

        q.get_results(sql_file="test_select.sql",
                      output_prefix="test_query",
                      )

        query = open(os.path.join(self.tempdir, "test_select.sql")).read()

        self.assertIn("-- Ran query on:", query)
        self.assertIn("-- Parameters: {}", query)
        self.assertIn("SELECT * FROM test;", query)

    def test_athena(self):

        q = CachedAthenaQuery(params={"aws_region": self.region,
                                      "athena_db": self.athena_db_name},
                              cache_path=self.s3_bucket,
                              sql_paths=[self.sql_path],
                              sql_archive_path=self.tempdir)
        self.set_timeouts(q)

        days = [datetime.utcnow() - timedelta(days=1),
                datetime.utcnow(),
                datetime.utcnow() + timedelta(days=1)]

        # Get the results, refreshing the cache
        r = q.get_results(sql_file="athena_test.sql",
                          output_prefix="athena_test",
                          params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days]},
                          refresh_cache=True)

        # Get the output
        output = PandasDoggo().load_csv(r, compression="gzip")

        # Clear the cache again
        q.clear_cache(sql_file="athena_test.sql",
                      output_prefix="athena_test",
                      params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days]})
        self.assertFalse(q._exists(r))

        # check can still read in validation mode
        q.validation_mode = True
        s = q.get_results(sql_file="athena_test.sql",
                          output_prefix="athena_test",
                          params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days]})
        self.assertEqual(s, r)

        # check the archive.
        file = DoggoFileSystem().glob(os.path.join(self.tempdir, "athena_test*.sql"))[0]
        query = open(file).read()

        self.assertIn("-- Ran query on:", query)
        self.assertIn("-- Parameters: {'aws_region': 'us-west-2'", query)
        self.assertIn("SELECT current_date as dt, CAST(current_date as varchar) as day)", query)

        # check the results
        self.assertEqual(output.to_csv(index=False),
                         'dt,day\n{0:%Y-%m-%d},{0:%Y-%m-%d}\n'.format(datetime.utcnow()))

    def test_athena_replacement_dict(self):

        q = CachedAthenaQuery(params={"aws_region": self.region,
                                      "athena_db": self.athena_db_name},
                              cache_path=self.s3_bucket,
                              sql_paths=[self.sql_path], )
        self.set_timeouts(q)

        days = [datetime.utcnow() - timedelta(days=1),
                datetime.utcnow(),
                datetime.utcnow() + timedelta(days=1)]

        r = q.get_results(sql_file="athena_test.sql",
                          output_prefix="athena_test",
                          replacement_dict={
                              "{day_list}": "(" + ",".join(["'{0:%Y-%m-%d}'".format(day) for day in days]) + ")"})

        # Get the output
        output = PandasDoggo().load_csv(r, compression="gzip")

        # check the results
        self.assertEqual(output.to_csv(index=False),
                         'dt,day\n{0:%Y-%m-%d},{0:%Y-%m-%d}\n'.format(datetime.utcnow()))

    def test_s3_archive(self):

        q = CachedAthenaQuery(params={"aws_region": self.region,
                                      "athena_db": self.athena_db_name},
                              cache_path=self.s3_bucket,
                              sql_paths=[self.sql_path],
                              sql_archive_path=self.s3_bucket)
        self.set_timeouts(q)

        # use a list of length one
        days = [datetime.utcnow()]

        q.clear_cache(sql_file="athena_test.sql",
                      output_prefix="athena_test",
                      params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days]})
        try:
            DoggoFileSystem().rm(os.path.join(self.s3_bucket, "athena_test.sql"))
        except FileNotFoundError:
            pass

        q.get_results(sql_file="athena_test.sql",
                      output_prefix="athena_test",
                      params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days]})

        # check the archive.
        file = DoggoFileSystem().glob(os.path.join(self.s3_bucket, "athena_test*.sql"))[0]
        with DoggoFileSystem().open(file, "rb") as f:
            query = f.read().decode("UTF-8")

        self.assertIn("-- Ran query on:", query)
        self.assertIn("-- Parameters: {'aws_region': 'us-west-2'", query)
        self.assertIn("SELECT current_date as dt, CAST(current_date as varchar) as day)", query)

    def test_renaming_output_file(self):
        class MimicRedshiftQuery(CachedAthenaQuery):
            """
            Create a class that mimic's redshift's annoying functionality of producing differently named files
            """
            _redshift_checks = True

            def _execute_query(self,
                               query_file,
                               output_file,
                               query_parameters,
                               replacement_dict):
                return super(MimicRedshiftQuery, self)._execute_query(
                    query_file=query_file,
                    output_file=output_file + "000.gz",
                    query_parameters=query_parameters,
                    replacement_dict=replacement_dict
                )

        q = MimicRedshiftQuery(params={"aws_region": self.region,
                                       "athena_db": self.athena_db_name},
                               cache_path=self.s3_bucket,
                               sql_paths=[self.sql_path], )
        self.set_timeouts(q)

        days = [datetime.utcnow() - timedelta(days=1),
                datetime.utcnow(),
                datetime.utcnow() + timedelta(days=1)]

        r = q.get_results(sql_file="athena_test.sql",
                          output_prefix="athena_test",
                          replacement_dict={
                              "{day_list}": "(" + ",".join(["'{0:%Y-%m-%d}'".format(day) for day in days]) + ")"},
                          refresh_cache=True)

        # Get the output
        output = PandasDoggo().load_csv(r, compression="gzip")

        # check the results
        self.assertEqual(output.to_csv(index=False),
                         'dt,day\n{0:%Y-%m-%d},{0:%Y-%m-%d}\n'.format(datetime.utcnow()))

    def test_multiple_output_file(self):
        class MimicRedshiftQueryMultiple(CachedAthenaQuery):
            """
            Create a class that mimic's redshift upload multiple files
            """
            _redshift_checks = True

            def _execute_query(self,
                               query_file,
                               output_file,
                               query_parameters,
                               replacement_dict):
                a = super(MimicRedshiftQueryMultiple, self)._execute_query(
                    query_file=query_file,
                    output_file=output_file + "000.gz",
                    query_parameters=query_parameters,
                    replacement_dict=replacement_dict
                )

                DoggoFileSystem().cp(output_file + "000.gz", output_file + "001.gz")

                return a

        q = MimicRedshiftQueryMultiple(params={"aws_region": self.region,
                                               "athena_db": self.athena_db_name, },
                                       cache_path=self.s3_bucket,
                                       sql_paths=[self.sql_path], )
        self.set_timeouts(q)

        days = [datetime.utcnow()]

        q.clear_cache(sql_file="athena_test.sql",
                      output_prefix="athena_test",
                      replacement_dict={
                          "{day_list}": "(" + ",".join(["'{0:%Y-%m-%d}'".format(day) for day in days]) + ")"})

        r = q.get_results(sql_file="athena_test.sql",
                          output_prefix="athena_test",
                          replacement_dict={
                              "{day_list}": "(" + ",".join(["'{0:%Y-%m-%d}'".format(day) for day in days]) + ")"})

        # Get the output
        output = PandasDoggo().load_csv(r, compression="gzip")

        # check the results (should have double results)
        self.assertEqual(output.to_csv(index=False),
                         'dt,day\n{0:%Y-%m-%d},{0:%Y-%m-%d}\n{0:%Y-%m-%d},{0:%Y-%m-%d}\n'.format(datetime.utcnow()))

    def test_no_output_file(self):
        class MimicRedshiftQueryNone(CachedAthenaQuery):
            """
            Create a class that mimic's redshift upload multiple files
            """
            _redshift_checks = True

            def _execute_query(self,
                               query_file,
                               output_file,
                               query_parameters,
                               replacement_dict):
                a = super(MimicRedshiftQueryNone, self)._execute_query(
                    query_file=query_file,
                    output_file=output_file,
                    query_parameters=query_parameters,
                    replacement_dict=replacement_dict
                )

                DoggoFileSystem().rm(output_file)

                return a

        q = MimicRedshiftQueryNone(params={"aws_region": self.region,
                                           "athena_db": self.athena_db_name},
                                   cache_path=self.s3_bucket,
                                   sql_paths=[self.sql_path], )
        self.set_timeouts(q)

        days = [datetime.utcnow()]

        q.clear_cache(sql_file="athena_test.sql",
                      output_prefix="athena_test",
                      replacement_dict={
                          "{day_list}": "(" + ",".join(["'{0:%Y-%m-%d}'".format(day) for day in days]) + ")"})
        with self.assertRaisesRegex(ValueError,
                                    "No file returned by the query. Does it contain an UNLOAD command?"):
            q.get_results(sql_file="athena_test.sql",
                          output_prefix="athena_test",
                          replacement_dict={
                              "{day_list}": "(" + ",".join(["'{0:%Y-%m-%d}'".format(day) for day in days]) + ")"})

    def test_unescaped_percent(self):
        q = CachedPep249Query(self.sqlite_db,
                              cache_path=self.tempdir,
                              sql_paths=[self.sql_path],
                              sql_archive_path=self.tempdir,
                              gzip=False)
        self.set_timeouts(q)

        with self.assertRaisesRegex(ValueError, "contains unescaped % signs that will not run"):
            q.get_results(sql_file="test_select_bad.sql",
                          output_prefix="test_query",
                          )

    def test_athena_queue(self):

        q = CachedAthenaQuery(params={"aws_region": self.region,
                                      "athena_db": self.athena_db_name},
                              cache_path=self.s3_bucket,
                              sql_paths=[self.sql_path],
                              sql_archive_path=self.tempdir,
                              queue_queries=True)
        self.set_timeouts(q)

        days = [datetime.utcnow() - timedelta(days=1),
                datetime.utcnow(),
                datetime.utcnow() + timedelta(days=1)]

        # Clear any previously cached file
        r1 = q.get_results(sql_file="athena_test.sql",
                           output_prefix="athena_test",
                           params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days]},
                           refresh_cache=True)

        r2 = q.get_results(sql_file="athena_test.sql",
                           output_prefix="athena_test",
                           params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days]})

        r3 = q.get_results(sql_file="athena_test.sql",
                           output_prefix="athena_test",
                           params={"day_list": ['{0:%Y-%m-%d}'.format(day) for day in days[:2]]})

        q.wait_for_completion()

        self.assertEqual(r1, r2)
        dfs = DoggoFileSystem()
        self.assertTrue(dfs.exists(r1))
        self.assertTrue(dfs.exists(r2))
        self.assertTrue(dfs.exists(r3))

    def test_concatenating_files(self):
        q2 = BaseCachedQuery(cache_path=self.tempdir,
                             sql_paths=[self.sql_path],
                             gzip=True)
        output_file = os.path.join(self.tempdir, 'part.csv.gz')
        files = [os.path.join(self.test_dir, 'concatenating_files', e) for e in ['part1.csv.gz', 'part2.csv.gz']]
        pg = PandasDoggo()
        # read raw data before being deleted
        df_raw = pd.concat([pg.load_csv(file, compression='gzip') for file in files])
        q2._concatenate_files(output_file=output_file,
                              files=files,
                              deleting_files=False)
        df_new = pg.load_csv(output_file)
        self.assertEqual(df_raw.col1.sum(), df_new.col1.sum())


def mock_dfs_ls(*args, **kwargs) -> List[str]:
    test_data_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'query_tests', 'input')

    if kwargs['location'] == 's3://test-bucket/cache1':
        return [
            's3://test-bucket/cache/col1=val1/col2=val2/file1',
            's3://test-bucket/cache/col1=val1/col2=val2/file2',
            's3://test-bucket/cache/col1=val1/col2=val3/file3',
            's3://test-bucket/cache/col1=val2/col2=val2/file4'
        ]
    elif kwargs['location'] == 's3://test-bucket/cache2':
        return [
            os.path.join(test_data_dir, 'csv', 'file1.csv'),
            os.path.join(test_data_dir, 'csv', 'file2.csv')
        ]
    elif kwargs['location'] == 's3://test-bucket/cache3':
        return [
            os.path.join(test_data_dir, 'parquet', 'file1.parquet'),
            os.path.join(test_data_dir, 'parquet', 'file2.parquet')
        ]


class MockLock:
    def __enter__(self):
        return self

    def __exit__(self, *_):
        pass


class TestCachedCTASQuery(BaseTest):
    REGION = 'us-east-1'
    ATHENA_DB_NAME = 'sampledb'
    CTAS_DB_NAME = 'ctas_sampledb'
    S3_BUCKET = S3Location("s3://test-bucket/")

    def setUp(self) -> None:
        super().setUp()
        self.sql_path = os.path.join(self.test_dir, "query_tests")

        self.q = CachedCTASQuery(
            params={
                "aws_region": self.REGION,
                "athena_db": self.ATHENA_DB_NAME
            },
            cache_path=self.S3_BUCKET.join('cache'),
            sql_paths=[self.sql_path],
            sql_archive_path=self.tempdir
        )

    def test_local_cache_path_error(self) -> None:
        with self.assertRaisesRegex(AssertionError, "CTAS queries can only output to S3"):
            CachedCTASQuery(cache_path='/some/local/path')

    def test_get_query_header_basic(self) -> None:
        ctas_header = CachedCTASQuery.get_query_header(
            database='test_database',
            table_name='test_table',
            external_location=self.S3_BUCKET.join('test_ext_loc')
        )

        self.assertEqual(
            ctas_header,
            "CREATE TABLE test_database.test_table "
            "WITH("
            "format = 'PARQUET', "
            "write_compression = 'SNAPPY', "
            "external_location = 's3://test-bucket/test_ext_loc'"
            ") "
            "AS"
        )

    def test_get_query_header_csv_gzip_partitions(self) -> None:
        ctas_header = CachedCTASQuery.get_query_header(
            database='test_database',
            table_name='test_table',
            external_location=self.S3_BUCKET.join('test_ext_loc'),
            output_format='CSV',
            output_compression='GZIP',
            partition_columns="part_col_1"
        )

        self.assertEqual(
            ctas_header,
            "CREATE TABLE test_database.test_table "
            "WITH("
            "format = 'CSV', "
            "write_compression = 'GZIP', "
            "external_location = 's3://test-bucket/test_ext_loc', "
            "partitioned_by = ARRAY['part_col_1']"
            ") "
            "AS"
        )

    def test_get_query_header_bucket_info(self) -> None:
        ctas_header = CachedCTASQuery.get_query_header(
            database='test_database',
            table_name='test_table',
            external_location=self.S3_BUCKET.join('test_ext_loc'),
            bucket_info=("bucket_col_1", 3)
        )

        self.assertEqual(
            ctas_header,
            "CREATE TABLE test_database.test_table "
            "WITH("
            "format = 'PARQUET', "
            "write_compression = 'SNAPPY', "
            "external_location = 's3://test-bucket/test_ext_loc', "
            "bucketed_by = ARRAY['bucket_col_1'], "
            "bucket_count = 3"
            ") "
            "AS"
        )

    def test_get_query_header_full(self) -> None:
        ctas_header = CachedCTASQuery.get_query_header(
            database='test_database',
            table_name='test_table',
            external_location=self.S3_BUCKET.join('test_ext_loc'),
            partition_columns=["partition_col_1", "partition_col_2"],
            bucket_info=("bucket_col_1", 5)
        )

        self.assertEqual(
            ctas_header,
            "CREATE TABLE test_database.test_table "
            "WITH("
            "format = 'PARQUET', "
            "write_compression = 'SNAPPY', "
            "external_location = 's3://test-bucket/test_ext_loc', "
            "partitioned_by = ARRAY['partition_col_1', 'partition_col_2'], "
            "bucketed_by = ARRAY['bucket_col_1'], "
            "bucket_count = 5"
            ") "
            "AS"
        )

    @mock.patch('newtools.db.athena.AthenaClient.wait_for_completion', return_value=None)
    @mock.patch('newtools.db.athena.AthenaClient.add_query', return_value=None)
    @mock.patch('newtools.doggo.DoggoFileSystem.glob', return_value=[])
    @mock.patch('newtools.db.cached_query.BaseCachedQuery._get_lock', return_value=MockLock())
    @mock.patch('newtools.doggo.DoggoFileSystem.exists', side_effect=[False, False, True])
    def test_get_results(
        self,
        mock_dfs_exists,
        mock_ddb_lock,
        mock_dfs_glob,
        mock_ac_add_query,
        mock_ac_wait
    ) -> None:
        self.q.dfs.s3_client = mock.MagicMock()

        output = self.q.get_results(
            sql_file='ctas_test.sql',
            output_prefix='test_ctas',
            params={
                'ctas_athena_db': self.CTAS_DB_NAME,
                'partition_columns': 'col3',
                'bucket_info': ('col1', 3),
                'duration': 10
            }
        )

        self.assertEqual(
            output,
            's3://test-bucket/cache/test_ctas_953c27b0c2144a806647ac8156f5005f_8f3e959d729693471a83c7822c1b4884/'
        )
        self.assertEqual(mock_ac_wait.call_count, 2)

        self.q.dfs.s3_client.put_object.assert_not_called()

        add_query_calls = [
            mock.call(
                sql="CREATE TABLE ctas_sampledb.test_ctas_953c27b0c2144a806647ac8156f5005f_8f3e959d729693471a83c7822c1b4884 "
                    "WITH(format = 'PARQUET', write_compression = 'SNAPPY', "
                    "external_location = 's3://test-bucket/cache/test_ctas_953c27b0c2144a806647ac8156f5005f_8f3e959d729693471a83c7822c1b4884/', "
                    "partitioned_by = ARRAY['col3'], bucketed_by = ARRAY['col1'], bucket_count = 3) "
                    "AS\n"
                    "SELECT col1, col2, col3\n"
                    "FROM db_name.table_name\n"
                    "WHERE col2 >= 10",
                name='CTAS s3://test-bucket/cache/test_ctas_953c27b0c2144a806647ac8156f5005f_8f3e959d729693471a83c7822c1b4884/',
                output_location=S3Location('s3://test-bucket/cache')
            ),
            mock.call(
                sql='DROP TABLE IF EXISTS ctas_sampledb.test_ctas_953c27b0c2144a806647ac8156f5005f_8f3e959d729693471a83c7822c1b4884',
                name='DROP CTAS s3://test-bucket/cache/test_ctas_953c27b0c2144a806647ac8156f5005f_8f3e959d729693471a83c7822c1b4884/',
                output_location=S3Location('s3://test-bucket/cache')
            )
        ]
        mock_ac_add_query.assert_has_calls(add_query_calls)

    @mock.patch('newtools.db.athena.AthenaClient.wait_for_completion')
    @mock.patch('newtools.db.athena.AthenaClient.add_query')
    @mock.patch('newtools.doggo.DoggoFileSystem.glob', return_value=[])
    @mock.patch('newtools.db.cached_query.BaseCachedQuery._get_lock', return_value=MockLock())
    @mock.patch('newtools.doggo.DoggoFileSystem.exists', side_effect=[False, False, True])
    @mock.patch('newtools.db.athena.AthenaClient.get_query_result')
    def test_get_results_with_metadata(
        self,
        mock_get_result,
        *mock_args
    ) -> None:
        self.q.dfs.s3_client = mock.MagicMock()
        self.q.return_meta = True
        mock_get_result.return_value = pd.DataFrame(columns=['a', 'count'], data=[['a1', 50], ['a2', 100]])

        output = self.q.get_results(
            sql_file='ctas_test.sql',
            output_prefix='test_ctas',
            params={
                'ctas_athena_db': self.CTAS_DB_NAME,
                'duration': 15,
                'partition_columns': 'a'
            }
        )

        self.assertEqual(
            output,
            {
                'output_path': S3Location('s3://test-bucket/cache/test_ctas_953c27b0c2144a806647ac8156f5005f_030f8b8cda5e0bf5052b3db85b251678/'),
                'size': {
                    'a1': 50,
                    'a2': 100
                }
            }
        )

    @mock.patch('newtools.db.athena.AthenaClient.wait_for_completion', return_value=None)
    @mock.patch('newtools.db.athena.AthenaClient.add_query', return_value=None)
    @mock.patch('newtools.doggo.DoggoFileSystem.glob', return_value=[])
    @mock.patch('newtools.db.cached_query.BaseCachedQuery._get_lock', return_value=MockLock())
    @mock.patch('newtools.doggo.DoggoFileSystem.exists', side_effect=[False, False, False])
    def test_get_results_no_results(
        self,
        mock_dfs_exists,
        mock_ddb_lock,
        mock_glob,
        mock_ac_add_query,
        mock_ac_wait
    ) -> None:
        self.q.dfs.s3_client = mock.MagicMock()

        output = self.q.get_results(
            sql_file='ctas_test_no_data.sql',
            output_prefix='test_ctas_no_data',
            params={
                'ctas_athena_db': self.CTAS_DB_NAME,
                'partition_columns': 'col3',
                'bucket_info': ('col2', 1)
            }
        )

        self.assertEqual(
            output,
            's3://test-bucket/cache/test_ctas_no_data_c08d7a3da5df425dbbd09207ac9778f1_bce2f130a29bf7a4c07ff7b882b38eff/'
        )
        self.assertEqual(mock_ac_wait.call_count, 2)

        self.q.dfs.s3_client.put_object.assert_called_once_with(
            Body=b'',
            Bucket='test-bucket',
            Key='cache/test_ctas_no_data_c08d7a3da5df425dbbd09207ac9778f1_bce2f130a29bf7a4c07ff7b882b38eff/EMPTY'
        )

    @mock.patch('newtools.db.athena.AthenaClient.wait_for_completion', return_value=None)
    @mock.patch('newtools.db.athena.AthenaClient.add_query', return_value=None)
    @mock.patch('newtools.doggo.DoggoFileSystem.glob', return_value=[])
    @mock.patch('newtools.db.cached_query.BaseCachedQuery._get_lock', return_value=MockLock())
    @mock.patch('newtools.doggo.DoggoFileSystem.exists', return_value=False)
    def test_get_results_queue_queries(
        self,
        mock_dfs_exists,
        mock_ddb_lock,
        mock_dfs_glob,
        mock_ac_add_query,
        mock_ac_wait
    ) -> None:
        self.q.queue_queries = True
        for query_name in ['ctas_test', 'ctas_test_2']:
            self.q.get_results(
                sql_file=f'{query_name}.sql',
                output_prefix='test_ctas',
                params={
                    'ctas_athena_db': self.CTAS_DB_NAME,
                    'partition_columns': 'col3',
                    'bucket_info': ('col1', 3),
                    'duration': 10
                }
            )

        mock_ac_wait.assert_not_called()
        self.assertEqual(mock_ac_add_query.call_count, 2)

    def test_table_name_clean(self) -> None:
        output = self.q.extract_ctas_parameters(
            params={},
            output_file=self.S3_BUCKET.join('cache', '.._.._get_results_abc!-_123/')
        )
        self.assertEqual(
            output['table_name'],
            '__get_results_abc_123'
        )

    def test_extract_ctas_parameters(self) -> None:
        output = self.q.extract_ctas_parameters(
            params={
                "ctas_athena_db": self.ATHENA_DB_NAME,
                "output_format": "PARQUET",
                "output_compression": "GZIP",
                "partition_columns": ["col1", "col2"],
                "bucket_info": ("col3", 2)
            },
            output_file=self.S3_BUCKET.join('cache', 'get_results_abc_123/')
        )

        self.assertEqual(
            output,
            {
                'database': 'sampledb',
                'table_name': 'get_results_abc_123',
                'external_location': S3Location('s3://test-bucket/cache/get_results_abc_123/'),
                'output_format': 'PARQUET',
                'output_compression': 'GZIP',
                'partition_columns': ['col1', 'col2'],
                'bucket_info': ('col3', 2)
            }
        )

    def test_validate_ctas_params(self) -> None:
        with self.assertRaisesRegex(AssertionError, "Required CTAS parameter 'ctas_athena_db' has not been specified"):
            self.q.validate_ctas_params({})

    @mock.patch('newtools.doggo.DoggoFileSystem.ls', side_effect=mock_dfs_ls)
    def test_load_dir_csv(
        self,
        mock_ls
    ) -> None:
        load_path = self.S3_BUCKET.join('cache2')
        df_out = self.q.load_dir(load_path, file_format='csv')
        df_exp = pd.read_csv(os.path.join(self.sql_path, 'input', 'csv', 'expected.csv'))
        self._assert_frame_equal(df_out, df_exp)

    @mock.patch('newtools.doggo.DoggoFileSystem.ls', side_effect=mock_dfs_ls)
    def test_load_dir_parquet(
        self,
        mock_ls
    ) -> None:
        load_path = self.S3_BUCKET.join('cache3')
        df_out = self.q.load_dir(load_path)
        df_exp = pd.read_parquet(os.path.join(self.sql_path, 'input', 'parquet', 'expected.parquet'))
        self._assert_frame_equal(df_out, df_exp)

    def test_load_dir_iter_not_implemented(self) -> None:
        with self.assertRaisesRegex(
            NotImplementedError,
            re.compile("PandasDoggo.load_dir_iter only supports loading CSV or PARQUET files, not JSON", re.S)
        ):
            load_iter = CachedCTASQuery().load_dir_iter(load_path='', file_format='JSON')
            next(load_iter)

    @mock.patch('newtools.doggo.DoggoFileSystem.ls', side_effect=mock_dfs_ls)
    def test_get_partitions(
        self,
        mock_ls
    ) -> None:
        load_path = self.S3_BUCKET.join('cache1')
        partitions = CachedCTASQuery().get_partitions(load_path)

        self.assertEqual(
            sorted(partitions),
            [
                ('col1=val1', 'col2=val2', 's3://test-bucket/cache/col1=val1/col2=val2/'),
                ('col1=val1', 'col2=val3', 's3://test-bucket/cache/col1=val1/col2=val3/'),
                ('col1=val2', 'col2=val2', 's3://test-bucket/cache/col1=val2/col2=val2/')
            ]
        )
        mock_ls.assert_called_once_with(location=S3Location('s3://test-bucket/cache1'), recursive=True)

    def test_format_df_to_dict_total(self) -> None:
        df = pd.DataFrame(columns=['count'], data=[137])
        output_dict = CachedCTASQuery().format_df_to_dict(df)
        self.assertEqual(output_dict, {'total': 137})

    def test_format_df_to_dict_one_column(self) -> None:
        df = pd.DataFrame(columns=['a', 'count'], data=[['a1', 100], ['a2', 200]])
        output_dict = CachedCTASQuery().format_df_to_dict(df, index_cols=['a'])
        self.assertEqual(output_dict, {'a1': 100, 'a2': 200})

    def test_format_df_to_dict_multiple_columns(self) -> None:
        df = pd.DataFrame(columns=['a', 'b', 'count'], data=[['a1', 'b1', 100], ['a2', 'b2', 200]])
        output_dict = CachedCTASQuery().format_df_to_dict(df, index_cols=['a', 'b'])
        self.assertEqual(output_dict, {('a1', 'b1'): 100, ('a2', 'b2'): 200})

    @mock.patch('newtools.AthenaClient.get_query_result')
    @mock.patch('newtools.AthenaClient.wait_for_completion')
    @mock.patch('newtools.AthenaClient.add_query')
    def test_get_query_metadata_with_partitions(
        self,
        mock_add,
        mock_wfc,
        mock_get
    ) -> None:
        mock_get.return_value = pd.DataFrame(columns=['a', 'count'], data=[['a1', 100]])

        self.q.get_query_metadata(
            database='test_db',
            table_name='test_table',
            partition_columns=['a'],
        )

        mock_add.assert_called_once_with(
            """SELECT a, COUNT(1) FROM test_db.test_table GROUP BY 1""",
            output_location=S3Location('s3://test-bucket/cache')
        )
        self.assertEqual(
            self.q.query_meta['size'],
            {'a1': 100}
        )

    @mock.patch('newtools.AthenaClient.get_query_result')
    @mock.patch('newtools.AthenaClient.wait_for_completion')
    @mock.patch('newtools.AthenaClient.add_query')
    def test_get_query_metadata_without_partitions(
        self,
        mock_add,
        mock_wfc,
        mock_get
    ) -> None:
        mock_get.return_value = pd.DataFrame(columns=['count'], data=[100])

        self.q.get_query_metadata(
            database='test_db',
            table_name='test_table'
        )

        mock_add.assert_called_once_with(
            """SELECT  COUNT(1) FROM test_db.test_table """,
            output_location=S3Location('s3://test-bucket/cache')
        )
        self.assertEqual(
            self.q.query_meta['size'],
            {'total': 100}
        )
